#pragma once

struct interrupt_table {
	void (*non_maskable_interrupt)(void);
	void (*hard_fault)(void);
	void (*mpu_fault)(void);
	void (*bus_fault)(void);
	void (*usage_fault)(void);
	void (*reserved0)(void);
	void (*reserved1)(void);
	void (*reserved2)(void);
	void (*reserved3)(void);
	void (*svcall_handler)(void);
	void (*debug)(void);
	void (*reserved4)(void);
	void (*pendingsv_handler)(void);
	void (*system_tick)(void);
	void (*pss_interrupt)(void);
	void (*cs_interrupt)(void);
	void (*pcm_interrupt)(void);
	void (*wdt_interrupt)(void);
	void (*fpu_interrupt)(void);
	void (*flctl_interrupt)(void);
	void (*comparator0_interrupt)(void);
	void (*comparator1_interrupt)(void);
	void (*timer_a0_interrupt0)(void);
	void (*timer_a0_interrupt1)(void);
	void (*timer_a1_interrupt0)(void);
	void (*timer_a1_interrupt1)(void);
	void (*timer_a2_interrupt0)(void);
	void (*timer_a2_interrupt1)(void);
	void (*timer_a3_interrupt0)(void);
	void (*timer_a3_interrupt1)(void);
	void (*eusci_a0_interrupt)(void);
	void (*eusci_a1_interrupt)(void);
	void (*eusci_a2_interrupt)(void);
	void (*eusci_a3_interrupt)(void);
	void (*eusci_b0_interrupt)(void);
	void (*eusci_b1_interrupt)(void);
	void (*eusci_b2_interrupt)(void);
	void (*eusci_b3_interrupt)(void);
	void (*adc_interrupt)(void);
	void (*timer32_1_interrupt)(void);
	void (*timer32_2_interrupt)(void);
	void (*timer32_combined_interrupt)(void);
	void (*aes256_interrupt)(void);
	void (*rtc_interrupt)(void);
	void (*dma_error_interrupt)(void);
	void (*dma3_interrupt)(void);
	void (*dma2_interrupt)(void);
	void (*dma1_interrupt)(void);
	void (*dma0_interrupt)(void);
	void (*port1_interrupt)(void);
	void (*port2_interrupt)(void);
	void (*port3_interrupt)(void);
	void (*port4_interrupt)(void);
	void (*port5_interrupt)(void);
	void (*port6_interrupt)(void);
};

extern void bug(void);
extern void nop(void);
extern void reset(void);
extern void wait_for_interrupt(void);

#define INTERRUPT_TABLE(...) 					\
	_Pragma("GCC diagnostic push");				\
	_Pragma("GCC diagnostic ignored \"-Woverride-init\"");	\
	const struct interrupt_table interrupt_table		\
	__attribute__((section(".intvec"))) = {			\
	.non_maskable_interrupt = nop,				\
	.hard_fault = bug,					\
	.mpu_fault = bug,					\
	.bus_fault = bug,					\
	.usage_fault = bug,					\
	.reserved0 = bug,					\
	.reserved1 = bug,					\
	.reserved2 = bug,					\
	.reserved3 = bug,					\
	.svcall_handler = nop,					\
	.debug = nop,						\
	.reserved4 = bug,					\
	.pendingsv_handler = nop,				\
	.system_tick = nop,					\
	.pss_interrupt = nop,					\
	.cs_interrupt = nop,					\
	.pcm_interrupt = nop,					\
	.wdt_interrupt = nop,					\
	.fpu_interrupt = nop,					\
	.flctl_interrupt = nop,					\
	.comparator0_interrupt = nop,				\
	.comparator1_interrupt = nop,				\
	.timer_a0_interrupt0 = nop,				\
	.timer_a0_interrupt1 = nop,				\
	.timer_a1_interrupt0 = nop,				\
	.timer_a1_interrupt1 = nop,				\
	.timer_a2_interrupt0 = nop,				\
	.timer_a2_interrupt1 = nop,				\
	.timer_a3_interrupt0 = nop,				\
	.timer_a3_interrupt1 = nop,				\
	.eusci_a0_interrupt = nop,				\
	.eusci_a1_interrupt = nop,				\
	.eusci_a2_interrupt = nop,				\
	.eusci_a3_interrupt = nop,				\
	.eusci_b0_interrupt = nop,				\
	.eusci_b1_interrupt = nop,				\
	.eusci_b2_interrupt = nop,				\
	.eusci_b3_interrupt = nop,				\
	.adc_interrupt = nop,					\
	.timer32_1_interrupt = nop,				\
	.timer32_2_interrupt = nop,				\
	.timer32_combined_interrupt = nop,			\
	.aes256_interrupt = nop,				\
	.rtc_interrupt = nop,					\
	.dma_error_interrupt = nop,				\
	.dma3_interrupt = nop,					\
	.dma2_interrupt = nop,					\
	.dma1_interrupt = nop,					\
	.dma0_interrupt = nop,					\
	.port1_interrupt = nop,					\
	.port2_interrupt = nop,					\
	.port3_interrupt = nop,					\
	.port4_interrupt = nop,					\
	.port5_interrupt = nop,					\
	.port6_interrupt = nop,					\
	__VA_ARGS__						\
	};							\
	_Pragma("GCC diagnostic pop");

extern volatile unsigned * const nvic_interrupt_enable;
extern volatile unsigned short * const watchdog_control;
