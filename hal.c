#include "hal.h"
#include "gpio.h"

volatile unsigned * const nvic_interrupt_enable = (unsigned *)0xe000e104;
volatile unsigned short * const watchdog_control = (unsigned short *)0x4000480c;
volatile struct gpio * const gpio = (struct gpio *)0x40004c00;

void nop(void)
{
	;
}

void bug(void)
{
	gpio->port1.function0.pin0 = 0;
	gpio->port1.function1.pin0 = 0;
	gpio->port1.direction.pin0 = OUTPUT;
	gpio->port1.output.pin0 = 1;

	while (1) wait_for_interrupt();
}

void wait_for_interrupt(void)
{
	__asm__("wfi");
}

void disable_watchdog(void)
{
	unsigned char status = (*watchdog_control) | 0x80;
	*watchdog_control = status + 0x5a00;
}

extern int main(void);
void reset(void)
{
	disable_watchdog();
	main();
	bug();
}
