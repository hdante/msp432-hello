#pragma once

enum direction {
	INPUT, OUTPUT
};

enum edge {
	RISING, FALLING
};

enum resistor_pull {
	PULL_DOWN, PULL_UP
};

enum pin_bit {
	PIN0 = 0x1u,
	PIN1 = 0x2u,
	PIN2 = 0x4u,
	PIN3 = 0x8u,
	PIN4 = 0x10u,
	PIN5 = 0x20u,
	PIN6 = 0x40u,
	PIN7 = 0x80u
};

union pins {
	unsigned char value;
	struct {
		unsigned char pin0: 1;
		unsigned char pin1: 1;
		unsigned char pin2: 1;
		unsigned char pin3: 1;
		unsigned char pin4: 1;
		unsigned char pin5: 1;
		unsigned char pin6: 1;
		unsigned char pin7: 1;
	};
};

/* Port bytes overlap with each other, so add ignored bytes between each byte */
struct port {
	union pins input;
	unsigned char ignore0;
	union pins output;
	unsigned char ignore1;
	union pins direction;
	unsigned char ignore2;
	union pins resistor;
	unsigned char ignore3;
	union pins strength;
	unsigned char ignore4;
	union pins function0;
	unsigned char ingore5;
	union pins function1;
	unsigned char ingore6;
	/* Interrupt vector is not consistent, so hide it here */
	unsigned char garbage[8];
	union pins function_invert;
	unsigned char ignore7;
	union pins interrupt_edge;
	unsigned char ignore8;
	union pins interrupt_enable;
	unsigned char ignore9;
	union pins interrupt_pending;
};

struct gpio {
	union {
		struct port port1;
		struct {
			unsigned char pad0;
			struct port port2;
		};
		struct {
			/* Interrupt vectors are accessible from here */
			unsigned char ignore0[0xe];
			unsigned short port1_interrupt_value;
			unsigned char ignore1[0xe];
			unsigned short port2_interrupt_value;
		};
	};
	union {
		struct port port3;
		struct {
			unsigned char pad1;
			struct port port4;
		};
		struct {
			/* Interrupt vectors are accessible from here */
			unsigned char ignore2[0xe];
			unsigned short port3_interrupt_value;
			unsigned char ignore3[0xe];
			unsigned short port4_interrupt_value;
		};
	};
	union {
		struct port port5;
		struct {
			unsigned char pad2;
			struct port port6;
		};
		struct {
			/* Interrupt vectors are accessible from here */
			unsigned char ignore4[0xe];
			unsigned short port5_interrupt_value;
			unsigned char ignore5[0xe];
			unsigned short port6_interrupt_value;
		};
	};
	union {
		struct port port7;
		struct {
			unsigned char pad3;
			struct port port8;
		};
		struct {
			/* Interrupt vectors are accessible from here */
			unsigned char ignore6[0xe];
			unsigned short port7_interrupt_value;
			unsigned char ignore7[0xe];
			unsigned short port8_interrupt_value;
		};
	};
	union {
		struct port port9;
		struct {
			unsigned char pad4;
			struct port port10;
		};
		struct {
			/* Interrupt vectors are accessible from here */
			unsigned char ignore8[0xe];
			unsigned short port9_interrupt_value;
			unsigned char ignore9[0xe];
			unsigned short port10_interrupt_value;
		};
	};
};

extern volatile struct gpio * const gpio;
