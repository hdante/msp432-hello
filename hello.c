#include <stdbool.h>

#include "hal.h"
#include "gpio.h"

static void enable_button1(void)
{
	gpio->port1.direction.pin1 = INPUT;
	gpio->port1.output.pin1 = PULL_UP;
	gpio->port1.interrupt_edge.pin1 = FALLING;
	gpio->port1.resistor.pin1 = true;
	gpio->port1.interrupt_enable.pin1 = true;
	*nvic_interrupt_enable |= 8;
}

static void acknowledge_button1_interrupt(void)
{
	gpio->port1.interrupt_pending.pin1 = false;
}

static void enable_led1(void)
{
	gpio->port2.direction.pin1 = OUTPUT;
}

static void set_led1(bool value)
{
	gpio->port2.output.pin1 = value;
}

static void toggle_led1(void)
{
	gpio->port2.output.value ^= PIN1;
}

int main(void)
{
	enable_button1();
	enable_led1();
	set_led1(true);

	while(true) {
		wait_for_interrupt();
	}
}

static void on_port1_interrupt(void)
{
	acknowledge_button1_interrupt();
	toggle_led1();
}

INTERRUPT_TABLE(
	.port1_interrupt = on_port1_interrupt,
);
